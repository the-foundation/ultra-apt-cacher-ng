
[[ -z "$MIRROR_DOMAIN" ]] || {
for ubuvers in {14,16,18,20}.04 ;do
( docker run --rm -t ubuntu:$ubuvers  bash -c "apt update; apt-get --print-uris dist-upgrade -y ;apt-get -y --print-uris install --reinstall "'$(dpkg --get-selections |grep -v deinstall|cut -d" " -f1|cut -f1)'"|sed \"s/'//g\"|sed 's/\.deb/.deb\n/g'|grep http"|sed 's~://~://$MIRROR_DOMAIN/~g;s~http:~https:~g'|grep "^http" |while read a ;do curl $a &>/dev/null ;done ) &

done 
echo
; } ;
