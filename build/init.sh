#!/bin/bash 

[[ -z "$ACNGPASS" ]] && ACNGPASS=$(for rounds in $(seq 1 24);do tr -cd '[:alnum:]_\-.' < /dev/urandom  |head -c48;echo ;done|grep -e "_" -e "\-" -e "\."|grep ^[a-zA-Z0-9]|grep [a-zA-Z0-9]$|tail -n1)

[[ -z "$PROXYDOMAIN" ]] && PROXYDOMAIN=proxy.domain.tld

test -e /var/ww/html/ || mkdir -p /var/www/html
mkdir /var/www/favicon
echo "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="|base64 -d > /var/www/favicon/favicon.ico
ln -s /var/www/favicon/favicon.ico /var/www/favicon/favicon.png
ln -s /var/www/favicon/favicon.ico /var/www/favicon/apple-touch-icon.png



echo 'upstream acng_a {
    server 127.0.0.1:10001 fail_timeout=60s;
}

upstream acng_b {
    server 127.0.0.1:20001 fail_timeout=60s;
}

proxy_cache_path /tmp/.nginx_cache keys_zone=maincache:100m levels=1:2 inactive=1d max_size=256m;

include /etc/nginx/mapping.conf;   

server {
        resolver 4.2.2.4 8.8.8.8 valid=120s;
        listen 80 default_server;
        listen [::]:80 default_server;
 	    rewrite_log on ;
access_log   /tmp/nginx.access.$yearmonthday.$hour-$min.log main if=$loggable;
#access_log /dev/null;
error_log /dev/stderr notice;
#    error_page  403   https://httpstatuspages.github.io/403.html;
    error_page  403   /403.html;
    error_page  401   /403.html;
location = /healthcheck  {     include /etc/nginx/cors.conf;    add_header Content-Type text/plain;    return 200 "OK=ALIVE";    access_log off;     }
location = /ping         {     include /etc/nginx/cors.conf;    add_header Content-Type text/plain;    return 200 "OK=ALIVE";    access_log off;     }
location = /403.html {
        proxy_pass https://httpstatuspages.github.io;
}
location = /favicon.ico {
        root /var/www/favicon;
}
location = /favicon.png {
        root /var/www/favicon;
}
location = /apple-touch-icon.png {
        root /var/www/favicon;
}
location = /robots.txt {
   add_header Content-Type text/plain;
   return 200 "User-agent: *\nDisallow: /\n";
}
location = /sitemap.xml {
   add_header Content-Type application/xml;
   return 200 "<urlset xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n<url>\n<loc>https://the-foundation.gitlab.io/</loc>\n<lastmod>2023-05-23T23:42:23+00:00</lastmod>\n</url>\n</urlset>";
}
merge_slashes off;

location = /acng-report.html {
        proxy_pass http://127.0.0.1:3142;
        auth_basic "Restricted Content";
        auth_basic_user_file /etc/htpasswd;
        proxy_pass_request_headers on;
        # kill cache
        add_header Last-Modified $date_gmt;
        add_header Cache-Control "max-age=0, no-cache, no-store, must-revalidate";
        add_header Pragma "no-cache";
        if_modified_since off;
        expires off;
        etag off;
}
#cat scripts that do  e.g https://mycache.tld/mycache.tld/mycache.tld/ports.ubuntu.com
    location /'$PROXYDOMAIN'/ {
        rewrite ^/'$PROXYDOMAIN'(.*)$ $1 last;
    }       

    location = // {
        rewrite ^ /index.html permanent;
    }
    location = / {
        rewrite ^ /index.html permanent;
    }
    location /index.html {
        root /var/www/html;
        index index.html;
        proxy_cache       maincache;
        proxy_cache_key   $uri;
        proxy_cache_valid 200 206 3000h;
    }
    location / {

        #access_log /dev/null;
        proxy_pass http://acng_a;
       # proxy_next_upstream error timeout http_500 http_502 http_503 http_504 non_idempotent;
        error_page 500 502 503 504 = @fallback;
        proxy_cache       maincache;
        proxy_cache_key   $uri;
        proxy_cache_valid 200 206 3000h;
    }
    location @fallback {
        #access_log /dev/null;
        proxy_pass http://acng_b;
        #proxy_next_upstream error timeout http_500 http_502 http_503 http_504 non_idempotent;
        error_page 500 502 503 504 = @final;
        proxy_cache       maincache;
        proxy_cache_key   $uri;
        proxy_cache_valid 200 206 3000h;
    }
    location @final {
        
        #rewrite ^(/'$PROXYDOMAIN')?(.*)$ $1 break;
        #proxy_pass http://127.0.0.1:3142;
        proxy_pass http://$uri$is_args$args;
        error_page 301 302 307 = @handle_redirect;
        #access_log /dev/null;
        proxy_cache       maincache;
        proxy_cache_key   $uri;
        proxy_cache_valid 200 206 3000h;
    }
    location @handle_redirect {
        resolver 8.8.8.8;
       #store the current state of the world so we can reuse it in a minute
       # We need to capture these values now, because as soon as we invoke 
       # the proxy_* directives, these will disappear
       set $original_uri $uri;
       set $orig_loc $upstream_http_location;

       # nginx goes to fetch the value from the upstream Location header
       proxy_pass $orig_loc;
       proxy_cache       maincache;
       # But we store the result with the cache key of the original request URI
       # so that future clients dont need to follow the redirect too
       proxy_cache_key $original_uri;
       proxy_cache_valid 200 206 3000h;
    }
}
' > /etc/nginx/sites-enabled/default
(
wget -c -O- http://mirrors.ubuntu.com/mirrors.txt 
wget -c -O- http://mirrors.ubuntu.com/mirrors.txt 
wget -c -O- http://mirrors.ubuntu.com/mirrors.txt 
) |sort -u |tee /etc/apt-cacher-ng/ubuntu_mirrors > /etc/apt-cacher-ng/backends_ubuntu &

chmod 777 /var/cache/apt-cacher-ng  ;
echo $UPSTREAM_PROXY|wc -w|grep -q ^0|| ( 
    grep ^Proxy /etc/apt-cacher-ng/acng.conf -q && sed 's/^Proxy.\+//g' /etc/apt-cacher-ng/acng.conf -i; 
    echo "Proxy: $UPSTREAM_PROXY" > /etc/apt-cacher-ng/acng.conf )  ;

chmod 777 /var/cache/apt-cacher-ng;

echo "HTPASSWD::: BACKEND USER: acng | BACKEND PASSWORD: $ACNGPASS"

htpasswd -cbB -C 10 /etc/htpasswd acng "$ACNGPASS" &

grep -q "^UserAgent: Debian APT-HTTP"  /etc/apt-cacher-ng/acng.conf || (
echo "UserAgent: Debian APT-HTTP/1.3 (1.6.10)" >> /etc/apt-cacher-ng/acng.conf
)

grep AdminAuth  /etc/apt-cacher-ng/acng.conf -q || (
    #psw="$RANDOM"_aptCache_"$RANDOM"
    psw=$ACNGPASS
    echo "SETTING RANDOM ACNG ADMIN PASS : $psw"
    echo "AdminAuth: acng:$psw" >>  /etc/apt-cacher-ng/acng.conf 
) 
## add-on fork
(
(
    (
        echo '#!/bin/bash'
        echo "while (true);do find  /tmp/ -name 'nginx.access.*log' |wc -l |grep -q '^0$' ||  timeout 58 bash -c 'tail -f /tmp/nginx.access.*log';sleep 2;done") > /usr/bin/logread
        chmod +x /usr/bin/logread 
) &
 
## tip from https://linux.debian.bugs.dist.narkive.com/0GNV3PoP/bug-930868-apt-cacher-ng-cron-daily-script-always-fails-with-502-connection-closed
( sleep 120; 
  while (true);do  
    curl --max-time 1800 --connect-timeout 15 -kLv "http://127.0.0.1:3142/acng-report.html?abortOnErrors=aOe&ignoreTradeOff=iTO&byPath=bP&byChecksum=bS&truncNow=tN&incomAsDamaged=iad&doExpire=Start+Scan+and%2For+Expiration&calcSize=cs&asNeeded=an#bottom" &>/tmp/1_scan.log;
    curl --max-time 1800 --connect-timeout 15 -kLv "http://127.0.0.1:3142/acng-report.html?abortOnErrors=aOe&justRemoveDamaged=Delete+damaged&calcSize=cs&asNeeded=an#bottom" &>/tmp/2_clean.log;
sleep 3530 ;done  ) &

GREPFILTER="Broken pipe"
[[ "$DEBUGME" = "true" ]] && GREPFILTER="foostatement"


#while (true);do socat TCP-LISTEN:80,fork,reuseaddr TCP-CONNECT:127.0.0.1:3142 2>&1;sleep 2 ;done  &
while (true);do find /tmp -type f -mmin -15 -name "nginx.access*.log" -delete  ;sleep 59;done  &
) &

## main stuff
while (true);do 
    nginx -g 'daemon off;' 2>&1|while read -r stringy ;do 
       [[ $stringy =~ .*\ prematurely\ closed. ]] && [[ ! $stringy =~ .*127.0.0.1:.0001.* ]] && echo "$stringy" ;done;
       sleep 2 ;done  &
while (true);do socat TCP-LISTEN:10001,fork,reuseaddr TCP-CONNECT:127.0.0.1:3142 2>&1 |grep -v -e "DUMMYSTATEMENT" -e "$GREPFILTER" ;sleep 2 ;done  &
while (true);do socat TCP-LISTEN:20001,fork,reuseaddr TCP-CONNECT:127.0.0.1:3142 2>&1 |grep -v -e "DUMMYSTATEMENT" -e "$GREPFILTER" ;sleep 2 ;done  &    
while (true);do apt-cacher-ng -c /etc/apt-cacher-ng ForeGround=1 2>&1 ;done |grep --line-buffered -i -e warn -e "error"
